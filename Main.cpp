#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
	std::string example = "jsdhjghsdkghkdgshd";
	
	std::cout << example << "\n";
	std::cout << example.length() << "\n";
	std::cout << "First letter: " << example[1] << "\n";
	std::cout << "Last letter: " << example[example.length() - 1] << "\n";

	return 0;
}
